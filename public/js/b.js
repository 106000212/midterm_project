function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('change1');
        // Check user login
        if (user) {
            user_email = user.email;
            user_name=user.username;
            //var user_password = user.password;
            menu.innerHTML = user.email ;
            var txtUsername = document.getElementById('user');

            //var user_page = document.getElementById("change3");
            menu.addEventListener('click', function(){
                document.getElementById('body').innerHTML="<div align='center'><img src='js/user.png'  style='height:200px; width:200px;'></div><div align='center'><div><h6></h6><h4>Accont/Email: </h4><h6>"+user_email+"</h6></div><h4>Password: </h4><h6>secret</h6></div></div>";
            });

           
            var logout = document.getElementById("change2");
            logout.innerHTML ="<a herf='postpage.html'>Logout</a>";
            logout.addEventListener('click', function(){
                firebase.auth().signOut();
            });
            show_post_list();
        } else {
            // It won't show any post if not login
            menu.innerHTML = "";
            document.getElementById('hide').innerHTML="";
            document.getElementById('article-list').innerHTML = "<div align='center' style=' display:flex;width:100%;height:200px;align-items:center;justify-content:center;'><h1 class='infinite animated rotateIn' style='color:gray;'>Login First!</h1></div>";
        }
    });

}


window.onload = function () {
    init();
};

function show_post_list(){

    var post_item = "<div class='modal fade' id='newPostModel' tabindex='-1' role='dialog' aria-labelledby='newPostModel' aria-hidden='true'><div class='modal-dialog' role='document'><div class='modal-content'><div class='modal-header'><h3 class='modal-title' id='modelTitle'>New Post</h3><button type='button' class='close' id='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span><a href='category.html'></a></button></div><div class='modal-body'><label for='newPostTitle'>Title</label><input class='form-control' id='newPostTitle'/><label for='newPostContent'>Content</label><textarea class='form-control' rows='10' id='newPostContent'></textarea></div><div class='modal-footer'><button type='button' class='btn btn-success' id='cancel' data-dismiss='modal'>Cancel</button><button type='button' class='btn btn-primary' id='post' data-dismiss='modal'>Post</button></div></div></div></div>";
    //var cancelbtn = document.getElementById('cancel');
    //var closebtn = document.getElementById('close');
    
    var postsRef = firebase.database().ref('post_listA');
    // List for store posts html
    var total_post = [];
    var post_list = [];
    
    postsRef.once('value')
        .then(function (snapshot) {
            snapshot.forEach(function(childSnapshot){
                var childData=childSnapshot.val();
                total_post[total_post.length]="<div class='media-body pb-3 mb-3  border-bottom border-blue ' ><div class='d-flex justify-content-between align-items-center '><a href='javascript:comment_handler(&quot;"+childData.value+"&quot;);'>"+childData.title+"</a><small>        by "+childData.email+"</small></div></div>\n";
            });
            post_list = total_post.reverse();
            document.getElementById('article-list').innerHTML=post_list.join('');
            new_post_btn = document.getElementById('newPostBtn');

            new_post_btn.addEventListener('click', function () {
                document.getElementById('article-list').innerHTML=post_item;
                post_btn = document.getElementById('post');
                post_title = document.getElementById('newPostTitle');
                post_txt = document.getElementById('newPostContent');
                post_btn.addEventListener('click', function () {
                    if (post_title.value != "" && post_txt.value != "") {
                        var d = new Date();
                        var num = Math.floor(Math.random()).toString();
                        var time = d.getFullYear().toString() + d.getMonth().toString() + d.getDate().toString() + d.getHours().toString() + d.getMinutes().toString() + d.getSeconds().toString();
                        firebase.database().ref('post_listA/').push({
                            title:post_title.value,
                            value:time+num,
                            email:firebase.auth().currentUser.email
                        });
                        firebase.database().ref('comment_listA/'+time+num+'/').push({
                            title:post_title.value,
                            message:post_txt.value,
                            email:firebase.auth().currentUser.email
                        });
                        post_title.value="";
                        post_txt.value="";
                        window.location = "postlistpageA.html";
                    }
                    else {
                        if (post_title.value == "" && post_txt.value != "") {
                            alert("without title");
                        }
                        else if (post_title.value != "" && post_txt.value == "") {
                            alert("without context");
                        }
                        else {
                            alert("nothing");
                        }
                    }
                });
            });
        
        })
        .catch(e => console.log(e.message));
}

function comment_handler(index) {
    document.getElementById('article-list').innerHTML="";
   
    var comment_item = "<div class='my-3 p-3 bg-white rounded box-shadow'><h5 class='border-bottom border-gray pb-2 mb-0'>Say Something</h5><textarea class='form-control' rows='3' id='comment' style='resize:none'></textarea><div class='media text-muted pt-3'><button id='comment_btn' type='button' class='btn btn-primary'>Send Comment</button></div></div>";
    // The html code for post
    
    var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><div class='media text-muted pt-3'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
    var str_after_content = "</p></div></div></div>\n";

    var commentsRef = firebase.database().ref('comment_listA/'+index.toString());
    // List for store posts html
    var total_comment = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;

    commentsRef.once('value')
        .then(function (snapshot) {
			snapshot.forEach(function(childSnapshot){
                var childData=childSnapshot.val();
                var str_first_floor = "<div class='my-3 p-3 bg-white rounded box-shadow'><h1 class='border-bottom border-gray pb-2 mb-0'>"+childData.title+"</h1><div class='media text-muted pt-3'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>"
                if (total_comment.length == 0) total_comment[total_comment.length]=str_first_floor+childData.email+"</strong>"+childData.message+str_after_content;
				else total_comment[total_comment.length]=str_before_username+childData.email+"</strong>"+childData.message+str_after_content;
				first_count+=1;
			});
            document.getElementById('article-list').innerHTML=total_comment.join('')+comment_item;
            comment_btn = document.getElementById('comment_btn');
            comment_txt = document.getElementById('comment');
            comment_btn.addEventListener('click', function () {
                if (comment_txt.value != "") {
                    firebase.database().ref('comment_listA/'+index.toString()+'/').push({
                        message:comment_txt.value,
                        email:firebase.auth().currentUser.email
                    });
                    comment_txt.value="";
                }
            });

			commentsRef.on('child_added',function(data){
				second_count+=1;
				if(second_count>first_count){
					var childData=data.val();
					total_comment[total_comment.length]=str_before_username+childData.email+"</strong>"+childData.message+str_after_content;
					document.getElementById('article-list').innerHTML=total_comment.join('')+comment_item;
                }
                
                comment_btn = document.getElementById('comment_btn');
                comment_txt = document.getElementById('comment');
                comment_btn.addEventListener('click', function () {
                    if (comment_txt.value != "") {
                        firebase.database().ref('comment_listA/'+index.toString()+'/').push({
                            message:comment_txt.value,
                            email:firebase.auth().currentUser.email
                        });
                        comment_txt.value="";
                    }
                });
			});
			
        })
        .catch(e => console.log(e.message));
};

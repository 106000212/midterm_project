var postLoadingTool = {
	postCount: 0,
	postLoadCount: 0,
	postKeyList: [],
	postDataList: [],
	/* reset the tool variable */
	setPostHTML: function (){
		// load post to HTML
		var articleList = document.querySelector("#article-list");
		articleList.innerHTML = postDataList.join("");
		
		// listen to comment buttom click
		postKeyList.forEach((key)=> {
			document.querySelector("#" + key + "-commentBtn").addEventListener("click", function(){				
				var commentContent = document.querySelector("#" + key + "-commentContent").value;
				if(commentContent!==""){
					// set comment info
					var today = new Date();
					var date = String(today.getFullYear()) + "/" + (today.getMonth()+1) + "/" + today.getDate();
					var user = firebase.auth().currentUser;
					var commentKey = firebase.database().ref().child("/posts/" + key + "comments").push().key;
					var newComment = {
						"author": user.displayName,
						"date": date,
						"content": commentContent
					}
					// update to database
					var updates = {};
					updates["/posts/" + key + "/comments/" + commentKey] = newComment;
					firebase.database().ref().update(updates)
						.then( () => {
							console.log("Comment successfully. comment ID: " + commentKey);
							loadPage();
						})
						.catch( e => {
							console.log(e.message)
						});
				}else {
					alert("Comment couldn't be empty!");
				}
			});
		});
	},
	addPostCount: function () {
		postCount++;
	},
	loadPost: function (postKey, postHTML){
		postLoadCount++;
		postKeyList.push(postKey);
		postDataList.unshift(postHTML);
		/* if all load has done */
		if(postLoadCount>=postCount){
			this.setPostHTML();
		}
	},
	resetTool: function (){
		postCount = 0;
		postLoadCount = 0;
		postKeyList = [];
		postDataList = [];
	}
}
/*
// update user data
function updateUserProfile(name, email, photoURL) {
	var user = firebase.auth().currentUser;
	if(user){
		var uid = user.uid;
		var data = {
			displayName: name,
			email: email,
			photoURL: photoURL
		};
		firebase.database().ref("/users/" + uid).set(data)
			.catch( e => { console.log(e.message); });
	}else {
		console.log("Fail to update user profile: couldn't access user data.")
	}
}

// user signup
function userSignup(){
	var name = document.querySelector("#displayName").value;
	var email = document.querySelector("#loginEmail").value;
	var password = document.querySelector("#loginPassword").value;
		
	//signup
	firebase.auth().createUserWithEmailAndPassword(email, password)
		.then(() => {
			console.log("Sign up successfully.");
			userLogin();
			var user = firebase.auth().currentUser;
			user.updateProfile({
				displayName: name,
				photoURL: "img/defaultUserPicture.jpg"
			})
			updateUserProfile(name, email, "img/defaultUserPicture.jpg");
			loadPage();
		})
		.catch(e => {
			console.log(e.message);
			alert("Something wrong. Please try again.");
		});
}

// user press logout button
function userLogout(){
	firebase.auth().signOut()
		.then(function(){
			alert("Sign out successfully!");
			loadPage();
		})
		.catch(e => {
			console.log(e.message);
			alert("Something wrong when logout. Please try again.");
		});
}


*/
function changeNavBar(type){
	var navInfo = document.querySelector("#userInfoNav");
	if(type==="login"){
		// login
		navInfo.innerHTML = 
		'<button class="btn-dark btn" type="button" data-toggle="dropdown">Login</button> \
		<div class="loginCard dropdown-menu" id="loginCard"> \
			<div class="container"> \
				<h3> Login </h3> \
				<div class="form-group"> \
					<label for="email">Email address</label> \
					<input type="email" class="form-control" id="loginEmail" required/> \
				</div> \
				<div class="form-group"> \
					<label for="password">Password</label> \
					<input type="password" class="form-control" id="loginPassword" required/> \
				</div> \
				<button class="btn btn-primary" id="loginBtn">Sign in</button> \
				<button class="btn btn-link" id="googleLogin"><img src="img/googleIcon.png" alt="google login"/></button> \
				<button class="btn btn-link" id="facebookLogin"><img src="img/facebookIcon.png" alt="facebook login"/></button> \
				<button class="btn btn-light" id="signupBtn">Sign up</button> \
			</div> \
		</div>';
		
		// listen login action
		document.querySelector("#loginBtn").onclick = userLogin;
		document.querySelector("#googleLogin").addEventListener("click", googleLogin);
		document.querySelector("#facebookLogin").addEventListener("click", facebookLogin);
		document.querySelector("#signupBtn").addEventListener("click", ()=>{
			changeNavBar("signup");
		});
		
	}else if(type==="logout"){
		var user = firebase.auth().currentUser;
		navInfo.innerHTML = 
			'<span id="userNavName">' + user.displayName + '</span> \
			<button class="btn-dark btn" type="button" id="logoutBtn">Logout</button>' ;
		
		// listen logout action
		document.querySelector("#logoutBtn").addEventListener("click", userLogout);
		
	}else if(type==="signup"){
		navInfo.innerHTML = 
		'<button class="btn-dark btn" type="button" data-toggle="dropdown">Sign up</button> \
		<div class="loginCard dropdown-menu" id="loginCard"> \
			<div class="container"> \
				<h3> Sign up </h3> \
				<div class="form-group"> \
					<label for="displayName">User name</label> \
					<input type="text" class="form-control" id="displayName" required/> \
				</div> \
				<div class="form-group"> \
					<label for="email">Email address</label> \
					<input type="email" class="form-control" id="loginEmail" required/> \
				</div> \
				<div class="form-group"> \
					<label for="password">Password</label> \
					<input type="password" class="form-control" id="loginPassword" required/> \
				</div> \
				<button class="btn btn-primary" id="signupBtn">Sign up</button> \
				<button class="btn btn-light" id="loginBtn">Sign in</button> \
			</div> \
		</div> ';
		
		document.querySelector("#signupBtn").onclick = userSignup;
		document.querySelector("#loginBtn").onclick = function(){
			changeNavBar("login");
		};
	}else {
		console.log("Fail to load navBar. Couldn't access user info.");
	}
}

// show sidebar info
function showUserInfo(user){
	var userInfo = document.querySelector(".userInfo");
	if(user){
		userInfo.innerHTML = 
		'<img class="img-thumbnail" id="userPhoto" src="' + user.photoURL + '"/> \
		 <a class="title" id="userName" href="userpage.html"> ' + user.displayName + ' </a> \
		 <p>' + user.email + '</p> \
		 <p id="userIntro"> Intro. </p>';
	}else{
		userInfo.innerHTML = 
		'<img class="img-thumbnail" id="userPhoto" src="img/defaultUserPicture.jpg"/> \
		<h3 class="title" id="userName"> Visitor </h3> \
		<p id="userIntro"> Welcome to <em>Post it</em>! </p>';
	}
}

/* generate comment HTML string */
function generateComment(commentList) {
	if(commentList){
		var commentHTMLList = [];
		for(commentKey in commentList){
			commentHTMLList.push(
				'<div class="comment-block"> \
					<p> ' + commentList[commentKey].content + '</p> \
					<p class="comment-author"> —— @' + commentList[commentKey].author + ' <span class="date"> ' + commentList[commentKey].date + '</span></p> \
				</div>');
		}
		return commentHTMLList.join("<hr/>");
	}
	else{
		return "";
	}
}

/* generate post as HTML string */
function generatePost(postKey) {
	// get post data
	firebase.database().ref("/posts/" + postKey).once("value")
		.then( function(snapshot){
			var authorName = snapshot.val().author;
			var authorEmail = snapshot.val().email;
			var date = snapshot.val().date;
			var title = snapshot.val().title;
			var content = snapshot.val().content;
			var comment = generateComment(snapshot.val().comments);
			
			var post = 
				'<div class="article"> \
					<h3 class="article-title">' + title + '</h3> \
					<p class="article-author">@' + authorName + ' <span class="email">(' + authorEmail + ')</span><span class="date">' + date + '</span></p> \
					<hr/> \
					<p class="article-content"><pre>' + content + '</pre></p> \
					<hr/> \
					<h5 class="comment-title">Comment</h5> \
					<div class="comment-list">' + comment + '</div> \
					<textarea class="form-control commentInput" rows="10" id="' + postKey + '-commentContent"> </textarea>  \
					<button class="btn btn-secondary btn-sm commentBtn" id="' + postKey + '-commentBtn"> Comment </button> \
				</div>';
			
			postLoadingTool.loadPost(postKey, post);
		})
		.catch(e => {
			console.log(e.message);
		});
}

function loadPost(user, type) {
	var postDataRef;
	var postKeyList = [];
	
	if(user){
		if(type==="all") {
			/* load all posts */
			postDataRef = firebase.database().ref("posts");
		}else if(type==="user") {
			/* only load user's posts */
			postDataRef = firebase.database().ref("/user-posts/" + user.uid);
		}
		
		postDataRef.once("value")
			.then( snapshot => {
				for(postKey in snapshot.val()){
					// record how many posts there are
					postLoadingTool.addPostCount();
					postKeyList.push(postKey);
				}
				
				if(postKeyList.length==0){
					// if no post
					var articleList = document.querySelector("#article-list");
					articleList.innerHTML = 
						'<div class="article"> \
							<p class="article-content"><pre> No posts recently. </pre></p> \
						</div>';
				}else{
					// get post from database
					postKeyList.forEach((postKey) => {
						generatePost(postKey);
					});
				}
			})
			.catch(e => {
				console.log(e.message);
			});
	}else {
		// user didn't login
		var post = document.querySelector("#article-list").innerHTML = 
			'<div class="article"> \
				<h3 class="article-title"> Welcome to <em>Post it</em>! </h3> \
				<hr/> \
				<p class="article-content"> You can post and comment freely here! </p>  \
			</div>';
	}
}

function loadPage() {
	var navInfo = document.querySelector("#userInfoNav");
	firebase.auth().onAuthStateChanged(function (user) {
		if(user){
			// if user logged in
			changeNavBar("logout");
			postLoadingTool.resetTool();
		}else{
			// if no user logged in
			changeNavBar("login");
		}
		showUserInfo(user);
		loadPost(user, "all");
	});
	resetInput();
	
	// clear all user input
	document.querySelectorAll("input").value = "";
}

/* if user generate new post */
document.querySelector("#post").onclick = function () {
	var user = firebase.auth().currentUser;
	if(user){
		var title = document.querySelector("#newPostTitle").value;
		var content = document.querySelector("#newPostContent").value;
		var today = new Date();
		var date = String(today.getFullYear()) + "/" + (today.getMonth()+1) + "/" + today.getDate();
		
		// get post key
		var newPostKey = firebase.database().ref().child("posts").push().key;
		// set post data
		var newPost = {
			"title": title,
			"author": user.displayName,
			"email": user.email,
			"date": date,
			"content": content,
			"comments": {}
		};
		
		// push post into database
		var updates = {};
		updates["/posts/" + newPostKey] = newPost;
		updates["/user-posts/" + user.uid + "/" + newPostKey] = true;
		firebase.database().ref().update(updates)
			.then(() => {
				console.log("New post: " + newPostKey);
				loadPage();
			})
			.catch( e => {
				console.log(e.message);
			});
	}else{
		// if user hadn't login
		alert("Please login first!");
	}
}

/* reset all inputs */
function resetInput(){
	var inputs = document.querySelectorAll("input");
	for(input in inputs){
		input.value = "";
	}
}

window.onload = function () {
    loadPage();
};

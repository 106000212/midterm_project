function initApp() {
    // Login with Email/Password
    var txtEmail = document.getElementById('email');
    var txtPassword = document.getElementById('pass');
    var txtEmail2 = document.getElementById('email2');
    var txtPassword2 = document.getElementById('pass2');
    var txtUsername = document.getElementById('user');
    var btnLogin = document.getElementById('btnLogin');
    var btnGoogle = document.getElementById('btngoogle');
    var btnFB = document.getElementById('btnFB');
    var btnSignUp = document.getElementById('btnSignUp');
    

    btnLogin.addEventListener('click', function () {
        var email = txtEmail2.value;
        var password = txtPassword2.value;
        var username = txtUsername.value;
        firebase.auth().signInWithEmailAndPassword(email,password).then(function(){
            txtEmail = email;
            txtPassword = password;
            txtUsername = username;
            window.location.href ="index.html";
        }

        ).catch(function(error){
            var errorCode= error.code;
            var errorMessage= error.message;
            create_alert("error",errorMessage);
            txtEmail.value = "";
            txtPassword.value = "";
            txtUsername.value="";
        }
        );



        /// TODO 2: Add email login button event
        ///         1. Get user input email and password to login
        ///         2. Back to index.html when login success
        ///         3. Show error message by "create_alert" and clean input field
    });
    var provider2 = new firebase.auth.GoogleAuthProvider();
    btnGoogle.addEventListener('click', function () {
        
        firebase.auth().signInWithPopup(provider2).then(function(result) {
            
            var token = result.credential.accessToken;
            var user = result.user;
            window.location.href ="index.html";

            }).catch(function(error) {
                var errorCode= error.code;
                var errorMessage= error.message;
                var email = error.email;
                var credential = error.credential;

                create_alert("error",errorMessage);
            });

        /// TODO 3: Add google login button event
        ///         1. Use popup function to login google
        ///         2. Back to index.html when login success
        ///         3. Show error message by "create_alert"
    });
    var provider1 = new firebase.auth.FacebookAuthProvider();
    btnFB.addEventListener('click', function () {
        
        firebase.auth().signInWithPopup(provider1).then(function(result) {
            
            var token = result.credential.accessToken;
            var user = result.user;
            window.location.href ="index.html";
            firebase.database().ref
            }).catch(function(error) {
                var errorCode= error.code;
                var errorMessage= error.message;
                var email = error.email;
                var credential = error.credential;

                create_alert("error",errorMessage);
            });

        /// TODO 3: Add FB login button event
        ///         1. Use popup function to login FB
        ///         2. Back to index.html when login success
        ///         3. Show error message by "create_alert"
    });
   
    btnSignUp.addEventListener('click', function () {        
        var email = txtEmail.value;
        var password = txtPassword.value;
        var username = txtUsername.value;
        firebase.auth().createUserWithEmailAndPassword(email,password).then(function(){
            create_alert("success","yes");
            txtEmail.value = "";
            txtPassword.value = "";
            
        }).catch(function(error) {
            var errorCode= error.code;
            var errorMessage= error.message;
            create_alert("error",errorMessage);
            txtEmail.value = "";
            txtPassword.value = "";
            });

            



        /// TODO 4: Add signup button event
        ///         1. Get user input email and password to signup
        ///         2. Show success message by "create_alert" and clean input field
        ///         3. Show error message by "create_alert" and clean input field
    });
}

// Custom alert
function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}

window.onload = function () {
    initApp();
};
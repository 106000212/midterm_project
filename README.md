# Software Studio 2019 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : [forum]
* Key functions (add/delete)
    1. [postpage]
    2. [postlistpage(登入前)]
    3. [postlistpage(登入後)]
    4. [userpage]
    5. [singin/up_page]
    6. [category_page]
    7. [leave_comment_on_post]
* Other functions (add/delete)
    1. [sign in up 翻轉介面]
    2. [Use CSS Animation]
    3. [Third-Party Sign In]

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

## Website Detail Description
我在Components Description會有仔細講解各頁面和功能
# 作品網址：[https://midproject-2a350.firebaseapp.com]

# Components Description : 
1. [postpage]: [在postlistpage的add new post 按鈕點擊後會跑出框框，此頁面即postpage，可輸入post title 和 內容，若按取消或x鍵可以關掉postpage，但就暫時無法看歷史post，只要之後有新post就可以看到整個post list]
<img src="rd3.jpg" ></img>
2. [postlistpage(登入前)]: [登入前的頁面，無法使用貼文或看貼文的基本功能，但有一個要求登入的css動畫，同時navbar中只能在此介面和連到登入介面 ]
<img src="rd7.jpg" ></img>
3. [postlistpage(登入後)]: [登入後的頁面，在等待歷史貼文載入前有個類似等待中的css動畫，當貼文出來後就會消失，有postbtn可供發布新貼文，並且發布後會留下發佈title和發布者email，另外不同的板上post是分開的 ]
<img src="rd2.jpg" ></img>
4. [userpage]: [此頁面可以看到註冊時的電子郵件，但為了安全性，不會給看密碼 ]
<img src="rd4.jpg" ></img>
5. [singin/up page]: [用來註冊和登入的頁面,可用註冊的帳戶或google、fb登入 ]
<img src="rd6.jpg" ></img>
6. [leave comment on post]: [ 點進各個貼文的標題，左上角會有標題內容，下方的文字欄可以發送評論，會即時顯示在下，並且會顯示發布者]
<img src="rd5.jpg" ></img>
7. [category_page]:[ 登入後第一個到的介面，可以選擇要到哪個討論版上去，未登入前點進去各版看不到東西]
<img src="rd1.jpg" ></img>
8. [Membership Mechanism] : [需要註冊並登入後才能使用各功能]
9. [Firebase Page] : [這次是使用firebase deploy 的page]
10. [Database] : [postlistpage的post及各post下的comment，即是用Database的各種功能]
11. [RWD] : [這次寫的時候有用bookstrap和css其他功能，使得各介面在手機或桌機下會以情況改變而不會使頁面跑掉]
<img src="rd8.jpg" ></img>


# Other Functions Description(1~10%) : 
1. [sign in up 翻轉介面] : [signin和signup的介面可直接點選對應字樣介面就會翻轉，並能正常發揮功能]
2. [Use CSS Animation] : [前面講過在postlistpage有兩種css動畫一種是類似等待畫面一種是提醒登入的動畫]
3. [Third-Party Sign In] : [可用google fb等第三方登入]


## API使用 :
大概就是LAB06用到的那幾個API和FB登入的
## Security Report (Optional):
1. 第一點是firebase Authentication的API功能若沒有登入為會員無法使用網頁的所有功能也看不到文章，能夠增加論壇本身的安全性不會讓匿名者使用
2. Posts和comments在發布後就無法再隨意更動
3. 網址開頭是https代表此網站相對值得信任及安全